from math import ceil

import matplotlib.pyplot as plt
import numpy as np

f_arr = []
data = []  # Array of (x, y) tuples
x_values = []
y_values = []
EPSILON = 1E-6


def clear_data_structs():
    f_arr.clear()
    data.clear()
    x_values.clear()
    y_values.clear()


def fill_f_arr(n: int = None, curr_pass: int = 0):
    global f_arr

    f_arr_x_values = None
    f_arr_y_values = None

    if n is None:
        n = len(x_values)
        f_arr_x_values = x_values
        f_arr_y_values = y_values
    else:
        f_arr_x_values = x_values[curr_pass * n:(curr_pass + 1) * n]
        f_arr_y_values = y_values[curr_pass * n:(curr_pass + 1) * n]

    # Prvo popuniti sa pocetnim vrijednostima - f[xk] = f(xk)
    for i in range(n):
        f_arr.append(f_arr_y_values[i])

    # Sada izracunati konkretne brojeve
    for i in range(1, n):
        for j in reversed(range(i, n)):
            denom = (f_arr_x_values[j] - f_arr_x_values[j - i])

            if abs(denom) <= EPSILON:
                # Ne mozemo dijeliti s 0 pa cemo to zanemariti
                continue
            else:
                f_arr[j] = (f_arr[j] - f_arr[j - 1]) / denom

    # Isprintati sto smo dobili
    print(f"Izracunati f_arr: {f_arr}")


def fill_data_generic(file_name):
    global data, x_values, y_values

    for line in open("data/" + file_name):
        parts = line.split()
        x = float(eval(parts[0]))
        y = float(eval(parts[1]))
        data.append((x, y))

    # Sortirati po x-ovima uzlazno
    data = sorted(data, key=lambda tuple_in: tuple_in[0])
    for (x, y) in data:
        x_values.append(x)
        y_values.append(y)


def fill_data_hooker_forbes(filter_duplicates:bool = False):
    """
    Ovi podaci predstavljaju povezanost vrelišta vode i atmosferskog tlaka.
    Njih su prošloga stoljeća izmjerili škotski fizičar James Forbes na raznim lokacijama
    u Škotskoj i Alpama, te dr. Joseph Hooker koji je svoje podatke izmjerio na Himalajama.

    Izmjereni su pedesetih godina 20. stoljeća, a sa svrhom razvoja jednostavne metode
    određivanja nadmorske visine planine.

    Na x osi je temperatura vrelišta, a na y osi atmosferski tlak.
    :return: None
    """
    global data, x_values, y_values

    for line in open("data/forbes.dat"):
        parts = line.split()
        x = float(parts[1])
        y = float(parts[0])
        data.append((x, y))

    for line in open("data/hooker.dat"):
        parts = line.split()
        x = float(parts[1])
        y = float(parts[0])
        data.append((x, y))

    # Sortirati po x-ovima uzlazno
    data = sorted(data, key=lambda tuple_in: tuple_in[0])
    for (x, y) in data:
        if filter_duplicates is True and x in x_values:
            continue

        x_values.append(x)
        y_values.append(y)


def algoritam_vrednovanja(x, n: int = None):
    """
    Veoma slicno Hornerovoj shemi
    :return: vrijednost polinoma pn(x)
    """

    if n is None:
        n = len(x_values)

    to_ret_sum = f_arr[n - 1]

    for i in reversed(range(n - 1)):
        to_ret_sum = to_ret_sum * (x - x_values[i]) + f_arr[i]

    return to_ret_sum


def main():
    print("Newtonov interpolacijski polinom")
    # fill_data_generic("tan_example_1.dat")
    # fill_data_hooker_forbes()
    fill_data_generic("forbes_hooker_example_1.dat")
    print(f"Pročitani podaci: {data}")

    # approximate_forbes_hooker_5_by_5()
    # return

    # Ajmo sada izracunati Newtonov polinom i predvidjeti te tocke
    fill_f_arr()

    x = 15.377
    res = algoritam_vrednovanja(x)
    print(f"Approx f({x}) = {res}")

    # Ajmo nacrtati taj polinom
    plot_newton_polynom()


def plot_newton_polynom():
    t = np.arange(15, 31, 1E-3)
    t_y = []

    for t_x in t:
        res = algoritam_vrednovanja(t_x)
        t_y.append(res)

    # Sada ih treba nacrtati
    plt.ylabel("y-axis")
    plt.xlabel("x-axis")
    plt.plot(t, t_y, "b-", x_values, y_values, 'ro')
    plt.axis([15, 31, 0, 250])
    plt.show()


def approximate_forbes_hooker_5_by_5():
    win_len = 5

    n = len(x_values)
    for i in range(ceil(n / 5)):
        f_arr.clear()
        window_x = []
        window_y = []

        for j in range(win_len):
            if (i * win_len + j) >= n:
                break

            window_x.append(x_values[i * win_len + j])
            window_y.append(y_values[i * win_len + j])

        # Oke, prozor je popunjen
        # Popuniti f_arr tj. izracunati podijeljene razlike
        fill_f_arr(win_len, i)

        # Sada treba izvrednovati polinom
        t = np.arange(window_x[0], window_x[win_len-1], 1E-3)
        t_y = []

        for t_x in t:
            res = algoritam_vrednovanja(t_x, win_len)
            t_y.append(res)

        # Nacrtaj ih, i idi dalje
        plt.plot(t, t_y, "b-", x_values, y_values, 'ro')
        plt.axis([15, 31, 0, 250])
        plt.show()
        print("One iteration passed!")


if __name__ == "__main__":
    main()
